require 'capybara';
require 'capybara/cucumber';
require 'selenium-webdriver'
require 'site_prism'
require 'rspec';
require_relative 'hooks.rb';

Capybara.configure do |config|
    config.default_driver = :selenium_chrome
    config.app_host = 'https://www.magazineluiza.com.br'
end
    Capybara.default_max_wait_time = 10