# Buscando produtos através do ícone de lupa
Quando('eu busco pelo produto {string}') do |produto|
  @campoBusca = find_by_id('inpHeaderSearch')
  @campoBusca.set(produto)
  @produtoBusca = produto
end

Quando('clico no ícone de lupa') do
  find_by_id('btnHeaderSearch').click
end

Então('deve realizar a busca do produto') do
  page.should have_content(@produtoBusca)
end

# Buscando produtos através da tecla enter
Quando('clico na tecla enter') do
  @campoBusca.send_keys :enter
end

# Buscando produto inexistente
Então('deve conter a frase que não foi possível encontrar a busca') do
  page.should have_content("Sua busca por #{@produtoBusca} não encontrou resultado algum")
  page.should have_content("Por favor, tente outra vez com termos menos específicos")
  
end

# Validando busca de Smartphone
Então ('as informações do produto devem estar de acordo com a busca') do
  page.should have_content(@produtoBusca)
end