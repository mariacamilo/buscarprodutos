#language: pt

Funcionalidade: Testar a busca de produtos no site Magazine Luiza
Para buscar um produto no site
Como usuário
Eu quero
    Validar o campo de busca
    Realizar busca de produtos
    Validar as informações dos produtos

Cenário: Buscando produtos através do ícone de lupa
    Quando eu busco pelo produto "Notebook Dell" 
    E clico no ícone de lupa
    Então deve realizar a busca do produto

Cenário: Buscando produtos através da tecla enter
    Quando eu busco pelo produto "Geladeira Brastemp" 
    E clico na tecla enter
    Então deve realizar a busca do produto

Cenário: Buscando produto inexistente
    Quando eu busco pelo produto "@%$**"
    E clico no ícone de lupa
    Então deve conter a frase que não foi possível encontrar a busca 

Esquema do Cenário: Validando busca de Smartphone
    Quando eu busco pelo produto "produto"
    E clico no ícone de lupa
    Então as informações do produto devem estar de acordo com a busca

    Exemplos:
    |produto|
    |Smartphone Samsung Galaxy A70|
    |Smartphone Motorola One Vision|
    |Smartphone Xiaomi Redmi Note 7|